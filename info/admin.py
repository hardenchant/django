from django.contrib import admin
from .models import Profile, MyFile
from django.contrib.auth.models import User

admin.site.register(MyFile)


class FileInLine(admin.TabularInline):
	model = MyFile

class ProfileAdmin(admin.ModelAdmin):
	inlines = [
		FileInLine,
	]
class UserAdmin(admin.ModelAdmin):
	pass

admin.site.register(Profile, ProfileAdmin)
