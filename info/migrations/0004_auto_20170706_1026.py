# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-06 10:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('info', '0003_remove_profile_customer'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='customer',
            field=models.TextField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='profile',
            name='files',
            field=models.FileField(blank=True, upload_to=''),
        ),
    ]
