# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-06 13:20
from __future__ import unicode_literals

from django.db import migrations, models
import info.models


class Migration(migrations.Migration):

    dependencies = [
        ('info', '0006_auto_20170706_1309'),
    ]

    operations = [
        migrations.AddField(
            model_name='myfile',
            name='upload',
            field=models.FileField(blank=True, upload_to=info.models.user_directory_path),
        ),
    ]
