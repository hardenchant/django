# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-06 13:58
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('info', '0011_auto_20170706_1355'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myfile',
            name='added_for',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='info.Profile'),
        ),
    ]
