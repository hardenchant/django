from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.added_for.user.id, filename)

class Profile(models.Model):
	user = models.OneToOneField(User)
	customer = models.CharField(max_length=100, blank=True)
	def __str__(self):
		return self.user.username

class MyFile(models.Model):
	name = models.CharField(max_length=100, blank=True)
	added_for = models.ForeignKey(Profile, null=True)
	upload = models.FileField(upload_to=user_directory_path, blank=True)
	def __str__(self):
		return self.name

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
	if created:
		Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
	instance.profile.save()
