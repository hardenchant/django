import json

def parse_qradar_json(path):
	f = open(path, 'r')
	str = f.read()
	jobj = json.loads(str)
	f.close()
	statistic = {}
	count = 0
	for struct in jobj:
		if struct['status'] in statistic:
			statistic[struct['status']][0] += 1
		else:
			statistic[struct['status']] = [1, ]
		count += 1
	for k in statistic.keys():
		statistic[k].append('%.2f' % (statistic[k][0] / count * 100))
	return {'statistic': statistic, 'count': count}

def parse_remedy_json(path):
	f = open(path, 'r')
	str = f.read()
	jobj = json.loads(str)
	f.close()
	statistic = {}
	count = 0
	for struct in jobj:
		if struct[1]['Status'] in statistic:
			statistic[struct[1]['Status']][0] += 1
		else:
			statistic[struct[1]['Status']] = [1, ]
		count += 1
	for k in statistic.keys():
		statistic[k].append('%.2f' % (statistic[k][0] / count * 100))
	return {'statistic': statistic, 'count': count}
