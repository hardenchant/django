from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^auth', views.auth, name='auth'),
	url(r'^download', views.download, name='download')
]
