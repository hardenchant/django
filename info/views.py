import os
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.conf import settings
from django.http import HttpResponse, Http404

from . import parsers

@login_required(login_url='/info/auth')
def download(request):
    file_path = os.path.join(settings.MEDIA_ROOT, 'user_' + str(request.user.id), request.GET['file'])
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/text")
            response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(file_path)
            return response
    raise Http404

def auth(request):
	if request.method == 'POST':
		user = authenticate(username=request.POST['username'], password=request.POST['password'])
		if user is not None:
			if user.is_active:
				login(request, user)
				return redirect('index')
			else:
				return render(request, 'info/auth.html', {'errors': ['Your account is disabled']})
		else:
			return render(request, 'info/auth.html', {'errors': ['Invalid pair login/password']})
	else:
		return render(request, 'info/auth.html')

@login_required(login_url='/info/auth')
def index(request):
	if request.method == 'POST':
		if request.POST['logout']:
			logout(request)
			return redirect('auth')
	else:
		info1 = parsers.parse_qradar_json('/home/lgv/Downloads/qradar.json')
		info2 = parsers.parse_remedy_json('/home/lgv/Downloads/remedy.json')
		
		user_path = os.path.join(settings.MEDIA_ROOT, 'user_' + str(request.user.id))
		file_list = os.listdir(user_path)

		return render(request, 'info/index.html', {'info': info1['statistic'].items(),
												   'count': info1['count'],
												   'info2': info2['statistic'].items(),
												   'count2': info2['count'],
												   'filelist': file_list,
												   'userpath': user_path,
												   })
